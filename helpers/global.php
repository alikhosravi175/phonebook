<?php

function assets($type, $file_name)
{
    echo ASSETS_URL . $type . '/' . $file_name;
}

function get_config($config_name)
{
    return include CONFIG_PATH . $config_name . '.php';
}

function get_thumbnail($img_url, $alt)
{
    return '<img src="' . $img_url . '" alt="' . $alt . '">';
}

function site_url($uri = null)
{
    return BASE_URL . $uri;
}

function jsRedirect($url, $secs)
{
    echo "
    <script>
         setTimeout(function(){
            window.location.href = '" . $url . "';
         }, " . $secs * 1000 . ");
      </script>";
}