<aside class="col-lg-3">
    <div class="box null">
        <div class="box-img">
            <img src="assets/images/1.png" alt="{first_name}">
        </div>
        <div class="details">
            <h2>ali khosravi</h2>
            <p>شما توسط این برنامه می توانید مخاطبین خود را مدیریت کنید. این برنامه امکاناتی مانند افزودن مخاطب،
                ویرایش مخاطب، دسته بندی مخاطبین و جستجو را دارد.</p>
        </div>
        <div class="icons">
            <div class="col-lg-6 pull-right">
                <a href="#" data-show="groups" class="showBox">
                    <div class="icon row">
                        <span class="icon-groups"></span>
                        <p>دسته ها</p>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 pull-right">
                <a href="#" data-show="add-group" class="showBox">
                    <div class="icon row">
                        <span class="icon-add"></span>
                        <p>افزودن دسته</p>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 pull-right">
                <a href="#">
                    <div class="icon row">
                        <span class="icon-profile"></span>
                        <p>پروفایل</p>
                    </div>
                </a>
            </div>
            <div class="col-lg-6 pull-right">
                <a href="?logged_out=true">
                    <div class="icon row back-red">
                        <span class="icon-exit"></span>
                        <p class="red">خروج</p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</aside>