<div id="add-contact" class="show-box add-style">
    <div class="title">
        <h2>افزودن مخاطب</h2>
    </div>
    <form class="add-group" action="add-contact" autocomplete="off" method="post" enctype="multipart/form-data">
        <div class="row-frm">
            <input class="form-control" type="text" name="full_name" placeholder="نام و نام خانوادگی">
        </div>
        <div class="row-frm">
            <input class="form-control" type="text" name="email" placeholder="آدرس ایمیل">
        </div>
        <div class="row-frm">
            <input class="form-control" type="text" name="phone" placeholder="شماره تماس">
        </div>
        <div class="row-frm marginB15">
            <select class="form-control" name="cat_id" id="cat_id">
                <option>دسته مورد نظر را انتخاب کنید...</option>
                <?php foreach ($data['categories'] as $category) :
                    echo '<option value="' . $category['cat_id'] . '">' . $category['cat_name'] . '</option>';
                endforeach; ?>
            </select>
        </div>
        <div class="row-frm marginB15">
            <input class="input-file" id="my-file" type="file" name="thumbanil">
            <label tabindex="0" for="my-file" class="input-file-trigger">
                <span class="fileSelect">افزودن فایل<i class="icon-attach"></i></span>
            </label>
        </div>
        <div class="text-center">
            <button class="btn btn-info">Submit</button>
        </div>
    </form>
</div>
<script>
    document.querySelector("html").classList.add('js');
    var fileInput = document.querySelector(".input-file"),
        button = document.querySelector(".input-file-trigger"),
        the_return = document.querySelector(".file-return");
    button.addEventListener("keydown", function(event) {
        if (event.keyCode == 13 || event.keyCode == 32) {
            fileInput.focus();
        }
    });
    button.addEventListener("click", function(event) {
        fileInput.focus();
        return false;
    });
    fileInput.addEventListener("change", function(event) {
        the_return.innerHTML = this.value;
    });
</script>