<div id="sort" class="">
    <div id="sort-box">
        <div class="title">
            <span class="icon-sort"></span>
            <span>مرتب سازی بر اساس :</span>
        </div>
        <ul class="list-action">
            <li><a href="#" class="sort" data-sort="newest">جدیدترین</a></li>
            <li><a href="#" class="sort" data-sort="past">قدیمی ترین</a></li>
            <li><a href="#" class="sort" data-sort="fav">موارد مورد علاقه</a></li>
            <li>
                <select class="sort-select " name="group_id">
                    <option>براساس دسته بندی</option>
                    <option>اقوام</option>
                    <option>همکاران</option>
                    <option>برنامه نویسان</option>
                </select>
            </li>
        </ul>

    </div>
</div>