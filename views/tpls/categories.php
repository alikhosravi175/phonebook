<div id="groups" class="show-box">
    <div class="container-groups null">
        <div class="title">
            <h2>لیست دسته ها</h2>
        </div>
        <?php foreach ($categories as $category) : 
            $num_rows = 10;
            ?>
        <div class="col-lg-3 pull-right">
            <div class="row-right">
                <div class="box">
                    <h2 data-id="{group_id}"><?php echo $category['cat_name'] ?></h2>
                    <p>تعداد اعضا</p>
                    <p><?php echo $num_rows; ?></p>
                    <div class="action">
                        <a href="<?php echo site_url('edit-category?category='.$category['cat_id']) ?>" class="edit">
                            <span class="icon-edit"></span>
                            <span>ویرایش</span>
                        </a>
                        <a href="<?php echo site_url('delete-category?category='.$category['cat_id']) ?>" class="delete-rows">
                            <span class="icon-delete"></span>
                            <span>حذف</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach ?>
    </div>
</div>