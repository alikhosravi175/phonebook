<div id="content" class="row">
    <?php
    foreach ($contacts as $contact) :
        foreach ($categories as $cat) {
            if ($cat['cat_id'] == $contact['cat_id']) {
                $category_name = $cat['cat_name'];
            }
        }
        // $category_name = 'خانواده';
        ?>
        <div class="col-lg-6 pull-right">
            <div class="box null">
                <div class="col-lg-6 pull-right">
                    <div class="box-img">
                        <?php echo get_thumbnail($contact['thumbnail'], $contact['full_name']); ?>
                    </div>
                </div>
                <div class="col-lg-6 pull-right">
                    <div class="row">
                        <div class="desc">
                            <h2><?php echo $contact['full_name'] ?></h2>
                            <p><?php echo $contact['phone'] ?></p>
                            <p><?php echo $contact['email'] ?></p>
                            <p><?php echo $category_name ?></p>
                        </div>
                        <div class="action">

                            <a href="#" class="update-situation"></a>

                            <a href="<?php echo site_url('edit-contact?contact=' . $contact['contact_id']) ?>" class="update-contact edit">
                                <span class="icon-edit"></span>
                                <span>ویرایش</span>
                            </a>
                            <a href="<?php echo site_url('delete-contact?contact=' . $contact['contact_id']) ?>" class="delete-rows">
                                <span class="icon-delete"></span>
                                <span>حذف</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
    <div class="col-lg-6 pull-right add">
        <a href="#" class="showBox" data-show="add-contact">
            <div class="box">
                <span class="icon-add"></span>
                <p>افزودن مخاطب</p>
            </div>
        </a>
    </div>
</div>