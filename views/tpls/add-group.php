<div id="add-group" class="show-box add-style">
    <div class="title">
        <h2>افزودن دسته</h2>
    </div>
    <form class="add-group" action="add-category" autocomplete="off" method="post">

        <div class="row-frm">
            <input class="form-control" type="text" name="cat_name" placeholder="نام دسته را وارد کنید.">
        </div>
        <div class="text-center">
            <button class="btn btn-info">ارسال</button>
        </div>
    </form>
    <div id="preLoader"></div>
</div>