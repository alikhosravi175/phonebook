<div id="update-contact" class="show-box add-style">
    <div class="title">
        <h2>ویرایش مخاطب</h2>
    </div>
    <form class="add-group" action="validating-information.php" autocomplete="off" method="post" enctype="multipart/form-data">
        <input type="hidden" value="{phone}" name="last_phone">
        <input type="hidden" value="{contact_id}" name="contact_id">
        <div class="row-frm">
            <input class="form-control" type="text" name="first_name" placeholder="نام" value="{first_name}">
        </div>
        <div class="row-frm">
            <input class="form-control" type="text" name="last_name" placeholder="نام خانوادگی" value="{last_name}">
        </div>
        <div class="row-frm">
            <input class="form-control" type="text" name="phone" placeholder="شماره تماس" value="{phone}">
        </div>
        <div class="row-frm">
            <input class="form-control" type="text" name="email" placeholder="آدرس ایمیل" value="{email}">
        </div>
        <div class="row-frm marginB15">
            <select class="form-control" name="group_id" id="group">
                <option>دسته مورد نظر را انتخاب کنید...</option>
                {groups_name}
            </select>
        </div>
        <div class="row-frm marginB15">
            <input class="input-file2" id="update_file" type="file" name="update_file">
            <label tabindex="0" for="update_file" class="input-file-trigger2">
                <span class="fileSelect2">افزودن فایل<i class="icon-attach"></i></span>
            </label>
        </div>
        <div class="text-center">
            <input class="btn btn-info" type="submit" name="update_contact">
        </div>
    </form>
</div>
<script>
    document.querySelector("html").classList.add('js');
    var fileInput = document.querySelector(".input-file2"),
        button = document.querySelector(".input-file-trigger2"),
        the_return = document.querySelector(".file-return");
    button.addEventListener("keydown", function(event) {
        if (event.keyCode == 13 || event.keyCode == 32) {
            fileInput.focus();
        }
    });
    button.addEventListener("click", function(event) {
        fileInput.focus();
        return false;
    });
    fileInput.addEventListener("change", function(event) {
        the_return.innerHTML = this.value;
    });
</script>