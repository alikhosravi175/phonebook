<?php
include_once VIEW_PATH . 'common/header.php';
echo '<div id="main"><div class="container"><section id="numbers" class="col-lg-9">';
include_once VIEW_PATH . "tpls/sort-box.php";
include_once VIEW_PATH . "tpls/members.php";
echo '</section>';
include_once VIEW_PATH . "common/sidebar.php";
echo '</div></div>';
include_once VIEW_PATH . "tpls/categories.php";
include_once VIEW_PATH . "tpls/add-group.php";
include_once VIEW_PATH . "tpls/add-contact.php";
include_once VIEW_PATH . "common/footer.php";
