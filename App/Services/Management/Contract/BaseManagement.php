<?php

namespace App\Services\Management\Contract;

abstract class BaseManagement
{
    protected $model;

    public function delete_management($id)
    {
        if (is_numeric($id)) {
            if (!(new $this->model)->destroy($id))
                return 'انجام نشد';
            return 'انجام شد';
        } else {
            return 'Faild';
        }
    }
    public function is_valid_file($file_data)
    {
        extract($file_data);
        $allowed_type = array('image/png', 'image/jpg', 'image/jpeg', 'application/pdf');
        if (!in_array($type, $allowed_type) || $size > 12 * 1024 * 1024)
            return false;
        return true;
    }
    public function save_file($file_name, $tmp_name)
    {
        $uploadFilePath = َUPLOAD_PATH . '/' . $file_name;
        return move_uploaded_file($tmp_name, $uploadFilePath);
    }
}
