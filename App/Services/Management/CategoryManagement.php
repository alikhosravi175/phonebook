<?php

namespace App\Services\Management;

use App\Core\Request;
use App\Services\Management\Contract\BaseManagement;

class CategoryManagement extends BaseManagement
{

    protected $model = "App\Models\Category";

    public function add_management($params)
    {
        if (!empty($params['cat_name'])) {
            if (!(new $this->model())->creat($params))
                return 'ناموفق';
            return 'دسته مورد نظر با موفقیت افزوده شد.';
        } else {
            return 'مقدار وارد شده نامعتبر است.';
        }
    }

    public function edit_management()
    { }
}
