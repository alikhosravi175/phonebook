<?php

namespace App\Services\Management;

use App\Models\Contact;
use App\Models\Category;
use App\Services\Management\Contract\BaseManagement;
use Respect\Validation\Validator;

class ContactManagement extends BaseManagement
{

    protected $model = "App\Models\Contact";

    public function add_management($params)
    {
        extract($params);
        extract($form_data);

        $message = array();
        $has_error = false;
        if (empty($full_name)) {
            $has_error = true;
            $message[] = 'نام را وارد کنید';
        }
        if (!Validator::email()->validate($email)) {
            $has_error = true;
            $message[] = 'ایمیل وارد شده صحیح نمی باشد.';
        }
        if (!Validator::phone()->validate($phone)) {
            $has_error = true;
            $message[] = 'شماره تماس وارد شده صحیح نمی باشد.';
        }
        if (!$this->is_valid_category($cat_id)) {
            $has_error = true;
            $message[] = 'دسته بندی انتخاب شده معتبر نیست.';
        }
        if ($file_data['size'] > 0) {
            if (!$this->is_valid_file($file_data)) {
                $has_error = true;
                $message[] = 'فایل ارسال شده معتبر نیست. حجم فایل باید کمتر از ۱۲mb باشد و فرمت آن png یا jpeg یا pdf می تواند باشد.';
            }
        }

        if (!$has_error) {
            if ($file_data['size'] > 0) {
                $file_extension = end(explode('.', $file_data['name']));
                $file_name = str_random(25) . '.' . $file_extension;
                $file_url = ($this->save_file($file_name, $file_data['tmp_name'])) ? UPLOADS_URL . $file_name : '';
            } else {
                $file_url = '';
            }
            $form_data['thumbnail'] = $file_url;
            if ((new $this->model())->creat($form_data)) {
                $message = 'اطلاعات با موفقیت ثبت شد.';
            }
        }
        return $message;
    }

    public function edit_management()
    { }

    public function is_valid_category($cat_id)
    {
        $categories = (new Category)->read('cat_id');
        if (!in_array($cat_id, $categories))
            return false;
        return true;
    }
}
