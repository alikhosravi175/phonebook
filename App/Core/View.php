<?php

namespace App\Core;

class View
{
    public static function load($path, $data = array())
    {
        $path = str_replace('.', DIRECTORY_SEPARATOR, $path);
        $full_path = VIEW_PATH . $path . '.php';
        if (file_exists($full_path) && is_readable($full_path)) {
            if (is_array($data)) {
                extract($data);
            }
            include $full_path;
        } else {
            echo 'Error: File View Not Found!';
        }
    }

    public static function render($path)
    {
        ob_start();
        self::load($path);
        return ob_get_clean();
    }
}
