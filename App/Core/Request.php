<?php

namespace App\Core;

use App\Utilities\Input;

class Request
{
    public $uri;
    public $method;
    public $params;
    public $referer;
    public $agent;
    public $ip;
    public $file_data;

    public function __construct()
    {

        $this->uri = rtrim(strtok(str_replace(SUB_DIRECTORY, '', $_SERVER['REQUEST_URI']), '?'), '/');
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->ip = $_SERVER['REMOTE_ADDR'];
        $this->agent = $_SERVER['HTTP_USER_AGENT'];
        $this->referer = $_SERVER['HTTP_REFERER'] ?? '';
        $this->params = (SANITIZE_ALL_DATA) ? (Input::sanitize($_REQUEST)) : $_REQUEST;
        $this->file_data = $_FILES['thumbanil'] ?? '';
    }

    public function key_exists($key)
    {
        return array_key_exists($key, $this->params);
    }

    public function method_exists($method_arr)
    {
        return in_array(strtolower($this->method), $method_arr);
    }

    public function param($key)
    {
        return $this->params[$key];
    }

    public function __get($key)
    {
        if ($this->key_exists($key)) {
            return $this->{$key} = $this->param($key);
        } else {
            return false;
        }
    }
}
