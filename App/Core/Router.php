<?php

namespace App\Core;

class Router
{

    public static $request;
    private static $routes;
    const base_controller = "\\App\\Controllers\\";
    const base_middlewares = "\\App\\Middlewares\\";

    public static function start()
    {
        self::$request = new Request;
        self::$routes = self::get_all_routes();
        if (!self::route_exists(self::$request->uri)) {
            header("HTTP/1.0 404 Not Found");
            View::load('errors.404');
            die();
        }
        if (!self::$request->method_exists(self::get_current_methods())) {
            header('HTTP/1.0 403 Forbidden');
            View::load('errors.403');
            die();
        }

        //check middlewares
        if (self::has_middleware()) {
            foreach (self::get_middlewares() as $middleware) {
                $middlewareClass = self::base_middlewares . $middleware;
                if (!class_exists($middlewareClass)) {
                    echo "Error: Class '$middlewareClass' was not found!";
                    die();
                }
                $middlewareObj = new $middlewareClass;
                $middlewareObj->handle(self::$request);
            }
        }

        list($controller_class, $controller_method) = explode('@', self::get_route_target());
        $controller_class = self::base_controller . $controller_class;
        if (class_exists($controller_class)) { 
            $controller_obj = new $controller_class;
            $controller_obj->$controller_method(self::$request);
        }
    }

    public static function get_all_routes()
    {
        return include BASE_PATH . 'routes/web.php';
    }

    public static function get_current_methods()
    {
        return explode('|', self::$routes[self::$request->uri]['method']);
    }

    public static function has_middleware()
    {
        return isset(self::$routes[self::$request->uri]['middleware']);
    }

    public static function get_middlewares()
    {
        if (GLOBAL_MIDDLEWARES) {
            return explode('|', self::$routes[self::$request->uri]['middleware'] . '|' . GLOBAL_MIDDLEWARES);
        }
        return explode('|', self::$routes[self::$request->uri]['middleware']);
    }

    public static function get_route_target()
    {
        return self::$routes[self::$request->uri]['target'];
    }

    public static function route_exists($current_uri)
    {
        return array_key_exists($current_uri, self::$routes);
    }
    
}
