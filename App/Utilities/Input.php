<?php

namespace App\Utilities;

class Input
{
    public static function sanitize($data)
    {
        $results = array();
        switch (gettype($data)) {
            case 'array':
                foreach ($data as $key => $value) {
                    $results[$key] = self::sanitize($value);
                }
                return $results;
            case 'object':
                foreach ($data as $key => $value) {
                    $results[$key] = self::sanitize($value);
                }
                return $results;
            case get_magic_quotes_gpc():
                return stripslashes($data);
            case "MySQL":
                return mysql_real_escape_string($data);
        }
        return strip_tags($data);
    }
}
