<?php

namespace App\Models;

use App\Models\Contract\BaseModel;

class Category extends BaseModel
{
    public static $table = CATEGORY_TABLE;
    public static $primary_key = 'cat_id';
}
