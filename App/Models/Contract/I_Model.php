<?php

namespace App\Models\Contract;

interface I_Model
{
    public function creat($data);
    public function read($columns, $where = null);
    public function update($data, $where);
    public function delete($where);
    public function query($sql);
}
