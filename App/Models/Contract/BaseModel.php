<?php

namespace App\Models\Contract;

use App\Models\Contract\I_Model;

abstract class BaseModel implements I_Model
{
    public static $conn;
    public static $table;
    public static $primary_key;

    public function __construct()
    {
        global $medoo;
        self::$conn = $medoo;
    }

    public function creat($data)
    {
        self::$conn->insert(static::$table, $data);
        return self::$conn->id();
    }
    public function read($columns = '*', $where = array())
    {
        return self::$conn->select(static::$table, $columns, $where);
    }

    public function update($data, $where)
    {
        $result = self::$conn->update(static::$table, $data, $where);
        return $result->rowCount();
    }

    public function delete($where)
    {
        $result = self::$conn->delete(static::$table, $where);
        return $result->rowCount();
    }

    public function query($query)
    {
        return self::$conn->query($query);
    }

    public function count($where = array())
    {
        return self::$conn->count(static::$table, $where);
    }

    public function destroy($id)
    {
        return $this->delete(array(static::$primary_key => $id));
    }
}
