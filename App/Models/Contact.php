<?php

namespace App\Models;

use App\Models\Contract\BaseModel;

class Contact extends BaseModel
{
    public static $table = CONTACT_TABLE;
    public static $primary_key = 'contact_id';
}
