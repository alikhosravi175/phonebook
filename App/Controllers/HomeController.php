<?php

namespace App\Controllers;

use App\Core\View;
use App\Core\Request;
use App\Models\Category;
use App\Models\Contact;
use App\Services\Management\CategoryManagement;
use App\Services\Management\ContactManagement;

class HomeController
{
    public function index(Request $requst)
    {
        $contacts = (new Contact())->read();
        $categories = (new Category())->read();
        $data = array(
            'contacts' => $contacts,
            'categories' => $categories
        );
        View::load('index', $data);
    }

    public function delete_contact(Request $request)
    {
        $message = (new ContactManagement())->delete_management($request->contact);
        View::load('message', $message);
    }

    public function delete_category(Request $request)
    {
        $message = (new CategoryManagement())->delete_management($request->category);
        View::load('message', $message);
    }

    public function add_contact(Request $request)
    {
        $message = (new ContactManagement())->add_management(
            array(
                'form_data' => $request->params,
                'file_data' => $request->file_data
            )
        );
        View::load('message', $message);
    }

    public function add_category(Request $request)
    {
        $message = (new CategoryManagement())->add_management($request->params);
        View::load('message', $message);
    }
}
