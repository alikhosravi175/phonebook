<?php

define('BASE_PATH', dirname(__DIR__) . DIRECTORY_SEPARATOR);
define('VIEW_PATH', BASE_PATH . 'views' . DIRECTORY_SEPARATOR);
define('َUPLOAD_PATH', BASE_PATH . 'uploads' . DIRECTORY_SEPARATOR);
define('CONFIG_PATH', BASE_PATH . 'configs' . DIRECTORY_SEPARATOR);
define('BASE_URL', 'http://localhost/myProject/phonebook/');
define('ASSETS_URL', 'http://localhost/myProject/phonebook/assets/');
define('UPLOADS_URL', 'http://localhost/myProject/phonebook/uploads/');

define('GLOBAL_MIDDLEWARES', false);
define('SUB_DIRECTORY', '/myProject/phonebook');
define('CONTACT_TABLE', 'contacts');
define('CATEGORY_TABLE', 'categories');
define('SANITIZE_ALL_DATA', 1);
define('IS_DEV_MODE', 1);
