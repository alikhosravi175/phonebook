<?php
session_start();

include_once 'bootstrap/constants.php';
include_once 'vendor/autoload.php';
include_once 'helpers/global.php';
include_once 'bootstrap/init.php';

App\Core\Router::start();
