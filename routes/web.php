<?php
return array(
    '' => [
        'method' => 'get',
        'target' => 'HomeController@index'
    ],
    '/' => [
        'method' => 'get',
        'target' => 'HomeController@index'
    ],
    '/delete-contact' => [
        'method' => 'get',
        'target' => 'HomeController@delete_contact'
    ],
    '/add-contact' => [
        'method' => 'post',
        'target' => 'HomeController@add_contact'
    ],
    '/delete-category' => [
        'method' => 'get',
        'target' => 'HomeController@delete_category'
    ],
    '/add-category' => [
        'method' => 'post',
        'target' => 'HomeController@add_category'
    ]
);
